# Video Editor

## About

A Backbone/Marionette demo application to browse/create/edit annotated
videos from youtube or vimeo.

It is intended to consume a server side api from the [video repo][vid]

[vid]:https://bitbucket.org/toonketels/video/src


## Prerequisites


To use with the server api node and npm are required.

You should also have mongo db installed and a database called `video` accessible
from localhost on default mongo port without username/password.

See [Mongodb docs](http://docs.mongodb.org/manual/installation) for installation instructions.


## How to set up?

Download the source and symlink `www` folder from the video repo's `public`
directory.

    // In your projects directory...
    cd /to/projects/dir

    // Download client side application code
    git clone git@bitbucket.org:toonketels/video-editor.git

    // Install dependencies
    cd video-editor
    npm install


    // Download server api code
    cd ..
    git clone git@bitbucket.org:toonketels/video.git

    // Install dependencies
    cd video
    npm install


    // Symlink apps
    cd ..
    ln -s video/public video-editor/www



## How to run application?

    // Start mongo
    mongo


    // Create video db (different terminal window)
    mongod
    use video


    // Start server (different terminal window)
    cd video
    node app.js

