/**
 * Our top level application.
 */
define(
  [
    'marionette',
    'services/vent',
    'services/error-logger',
    'services/router',
    'apps/header/header-app',
    'apps/editor/editor-app'
  ],

  function( Marionette, vent, errorLogger, router, HeaderApp, EditorApp ){

    // Start logging uncaught errors before anything else...
    errorLogger.start();


    // Initialize our app.
    var app = new Marionette.Application();


    /**
     * Add regions.
     */
    app.addRegions({
      'headerRegion': '#header',
      'mainRegion': '#main',
      'footerRegion': '#site'
    });


    /**
     * Code run when application starts.
     */
    app.addInitializer(function() {







      /**
       * Add modules.
       */
      app.apps = {};
      app.apps.headerApp = new HeaderApp({region: app.headerRegion});
      app.apps.editorApp = new EditorApp({region: app.mainRegion});

    });

    app.on('initialize:after', function() {
      if (Backbone.history) {
        Backbone.history.start({pushState: false});
        
        // Navigate to default route
        if (Backbone.history.fragment === "") {
          router.navigate('video', {trigger: true});
        }
      }
    });


    app.on('close', function() {
      app.apps.headerApp.close();
      app.apps.editorApp.close();
    });

    return app;
});