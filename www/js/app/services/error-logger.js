/**
 * Log Client side errors.
 *
 * Used for explicit logging of errors and uncaught
 * errors fired from window.onerror.
 *
 * @todo: Keep a list of errors in memory, every x time
 *        send it back to the server if we have some.
 *        Make an initial request to the server to get a uid for
 *        this user and add it to each http request header so
 *        we can match client/server side errors to the same
 *        user.
 */
define(function() {
  
  "use strict";


  var existingWindowOnErrorHandling,
      customHandlingActive = false;


  var logError = function(message) {
    console.log(message);
  };


  var handleErrors = function(message, file, line) {
    logError(file + ':' + line + '\n\n' + message);
  };


  /**
   * Our public api.
   */
  var errorLogging = {

    /**
     * Start logging uncaught errors.
     */
    start: function() {
      if (!customHandlingActive) {
        existingWindowOnErrorHandling = window.onerror;
        customHandlingActive = true;
        window.onerror = handleErrors;
      }
    },


    /**
     * Stop logging uncaught errors.
     */
    stop: function() {
      if (customHandlingActive) {
        window.onerror = existingWindowOnErrorHandling;
        customHandlingActive = false;
        existingWindowOnErrorHandling = null;
      }
    },


    /**
     * Explicit log function.
     */
    log: function(message) {
      logError(message);
    }
  };

  return errorLogging;
});