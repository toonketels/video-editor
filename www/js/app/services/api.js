/**
 * Specifies the public api.
 */
define(
  [
    '../apps/video/video-api'
  ],
  function( videoApi ) {

    "use strict";

    var api = {

      video: videoApi

    };

    window.api2 = api;

    return api;

});