/**
 * App State.
 */
define(
  [
    'backbone'
  ],
  function( Backbone ) {

    "use strict";

    var appState = new Backbone.Model();

    // Debug
    window.state = appState;

    return appState;
});