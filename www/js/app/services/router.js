/**
 * Define application routes and return router to navigate.
 */
define(
  [
    'backbone',
    'services/state'
  ],
  function( Backbone, state ) {

    "use strict";

    var AppRouter = Backbone.Router.extend({

      routes: {
        'video': 'list',
        'video/create': 'create',
        'video/:id/edit': 'update',
        'video/:id': 'show'
      },

      list: function() {
        state.set({
          currentRoute: 'video',
          currentAction: 'video:list'
        });
      },

      show: function(id) {
        state.set({
          currentRoute: 'video/'+id,
          currentAction: 'video:show',
          currentVideoDetail: id
        });
      },

      create: function() {
        state.set({
          currentRoute: 'video/create',
          currentAction: 'video:create'
        });
      },

      update: function(id) {
        state.set({
          currentRoute: 'video/'+id+'/edit',
          currentAction: 'video:update',
          currentVideoDetail: id
        });        
      }

    });

    var router = new AppRouter();

    return router;
});