require.config({
  urlArgs: "bust=" + (new Date()).getTime(),
  deps: ['main'],
  paths: {
    'backbone': '../vendor/backbone',
    'json2': '../vendor/json2',
    'underscore': '../vendor/underscore',
    'backbone.babysitter': '../vendor/backbone.babysitter',
    'backbone.paginator': '../vendor/backbone.paginator',
    'backbone.wreqr': '../vendor/backbone.wreqr',
    'backbone.queryparams': '../vendor/backbone.queryparams',
    'backbone-relational': '../vendor/backbone-relational',
    'marionette': '../vendor/backbone.marionette',
    'jquery': '../vendor/jquery-1.9.1',
    'text': '../vendor/text',
    'tpl': '../vendor/tpl',
    'popcorn': '../vendor/popcorn-complete',
    'bootstrap': '../vendor/bootstrap.min'
  },
  // Sets the configuration for your third party scripts
  // that are not AMD compatible
  shim: {
    'underscore': {
      exports: "_"
    },
    'backbone': {
      deps: ['json2', 'underscore', 'jquery'],
      exports: "Backbone"
    },
    'jquery': {
      exports: '$'
    },
    'backbone.paginator': {
      deps: ['backbone', 'underscore']
    },
    'backbone.queryparams': {
      deps: ['underscore', 'backbone']
    },
    'popcorn': {
      exports: 'Popcorn'
    },
    'bootstrap': {
      deps: ['jquery']
    }
  }
});