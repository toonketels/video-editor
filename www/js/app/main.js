// Require config used to go here
require(
  [
      'app'
  ],

  function( app ){

    "use strict";

    app.start();

    // Debug
    window.app = app;
 

});