/**
 * Header Application.
 *
 * Controls the header, displays the menu
 */
define(
  [
    'marionette',
    './views/header-layout',
    './controllers/menu-controller'
  ], 
  function( Marionette, HeaderLayout, MenuController ){

    "use strict";

    var HeaderApp = Marionette.Controller.extend({

      /**
       * App initialization.
       */
      initialize: function(options) {
        this.region = options.region;

        this.layout = this.initLayout();
        this.region.show(this.layout);

        this.menu = this.initMenu(this.layout.menuRegion);
      },


      /**
       * Initialize the layout used in the header.
       */
      initLayout: function() {
        return new HeaderLayout();
      },
  

      /**
       * Initialize the menu via its own controller.
       */
      initMenu: function(region) {
        return new MenuController({region: region});
      },


      /**
       * Teardown.
       */
      onClose: function() {
        this.layout.close();
        this.layout = null;
        this.region = null;

        this.menu.close();
        this.menu = null;
      }

    });

    return HeaderApp;
});