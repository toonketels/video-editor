/**
 * Layout for the header.
 */
define(
  [
    'marionette',
    'tpl!../templates/header-layout.tpl.html'
  ],
  function( Marionette, tpl ) {

    "use strict";

    var HeaderLayout = Marionette.Layout.extend({

      template: tpl,

      regions: {
        menuRegion: '#menu',
        topRegion: '#top-region'
      }

    });

    return HeaderLayout;

});