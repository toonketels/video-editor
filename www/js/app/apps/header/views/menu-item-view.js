/**
 * Menu item view.
 */
define(
  [
    'marionette',
    'tpl!../templates/menu-item-view.tpl.html',
    'services/router',
    'services/state'
  ],
  function( Marionette, tpl, router, state ) {

    "use strict";

    var MenuItemView = Marionette.ItemView.extend({

      tagName: 'li',

      template: tpl,

      events: {
        'click': 'select'
      },

      select: function(ev) {
        ev.preventDefault();
        router.navigate(this.model.get('route'), {trigger: true});
      },

      initialize: function() {
        this.listenTo(state, 'change:currentRoute', this.currentRouteChanged, this);
        this.listenTo(this.model, 'change:active', this.displayActiveState, this);
      },

      currentRouteChanged: function(state, value) {
        if (value === this.model.get('route')) {
          this.model.set('active', true);
        } else {
          this.model.set('active', false);
        }
      },

      displayActiveState: function(model, value) {
        if (value) {
          this.$el.addClass('active');
        } else {
          this.$el.removeClass('active');
        }

      }
      
    });

    return MenuItemView;
});