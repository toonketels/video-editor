/**
 * Menu view.
 */
define(
  [
    'marionette',
    './menu-item-view',
    'tpl!../templates/menu-view.tpl.html'
  ],
  function( Marionette, MenuItemView, tpl ) {

    "use strict";

    var MenuView = Marionette.CompositeView.extend({

      template: tpl,

      className: 'navbar navbar-inverse span12',

      itemViewContainer: 'ul.nav',

      itemView: MenuItemView

    });

    return MenuView;

});