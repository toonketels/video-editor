/**
 * Menu Controller.
 *
 * Initializes menu and controls its behavior.
 */
define(
  [
    'marionette',
    'backbone',
    '../views/menu-view'
  ],
  function( Marionette, Backbone, MenuView ){

    "use strict";

    var MenuController = Marionette.Controller.extend({

      /**
       * Initialization.
       */
      initialize: function(options) {
        this.region = options.region;

        this.menuCollection = this.initMenuCollection();
        this.menuView = this.initMenuView(this.menuCollection);
        this.region.show(this.menuView);

        window.header = this;
      },


      /**
       * Creates a collection of menu links.
       */
      initMenuCollection: function() {
        return new Backbone.Collection([
          {
            title: 'Show all videos',
            route: 'video'
          },
          {
            title: 'Create new video',
            route: 'video/create'
          }
        ]);
      },


      /**
       * Creates the menu view.
       */
      initMenuView: function(collection) {
        return new MenuView({collection: collection});
      },


      /**
       * Teardown.
       */
      onClose: function() {
        this.menuView.close();
        this.menuView = null;
        this.region = null;
        this.menuCollection = null;
      }

    });

    return MenuController;
});