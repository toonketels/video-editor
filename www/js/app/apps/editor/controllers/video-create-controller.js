/**
 * Video create controller.
 *
 * Controls how the video listing is displaying.
 */
define(
  [
    'marionette',
    '../views/video-create-view'
  ],
  function( Marionette, VideoCreateView ) {

    "use strict";

    var VideoCreateController = Marionette.Controller.extend({

      /**
       * Returns its view.
       */
      getView: function() {
        if (!this.view) {
          this.view = new VideoCreateView();
        }

        return this.view;
      },


      /**
       * Teardown.
       */
      onClose: function() {
        this.view.close();
      }

    });

    return VideoCreateController;

});