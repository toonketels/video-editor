/**
 * Video show controller.
 *
 * Controls how an individual video is displayed.
 */
define(
  [
    'marionette',
    'underscore',
    'services/api',
    'services/state',
    '../views/video-detail-view',
    '../views/video-detail-layout',
    '../views/video-admin-links-view'
  ],
  function( Marionette, _, api, state, VideoDetailView, VideoDetailLayout, VideoAdminLinksView ) {

    "use strict";

    var VideoShowController = Marionette.Controller.extend({

      /**
       * Initialization.
       */
      initialize: function(options) {
        this.initEventListening();
      },


      /**
       * Start listening for events.
       */
      initEventListening: function() {
        this.listenTo(state, 'change:currentVideoDetail', this.displayDetail, this);
      },


      /**
       * Returns its view.
       */
      getView: function() {
        if (!this.layout) {
          this.layout = new VideoDetailLayout();

          // Since we cannot show anything in our region unless the region
          // is rendered, we wait for it's dom:refresh event to display
          // the actual content.
          this.listenTo(this.layout, 'dom:refresh', this.displayDetail, this);

          // Only once...
          this.listenTo(this.layout, 'dom:refresh', this.displayAdminLinks, this);
        }

        return this.layout;
      },


      /**
       * Actually display the content.
       *
       * For every new detail video, a new view is initialized
       * and displayed.
       *
       * We debounce because in rare occasions, "dom:refresh" and
       * "change:currentvideoDetail" are fired rapidly after each other.
       */
      displayDetail: _.debounce(function() {

        // Initializes new view and removes reference
        // to previous view.
        var video = api.video.show(state.get('currentVideoDetail'));
        this.view = new VideoDetailView({model: video});

        // Closes the previous view and renderes new one.
        this.layout.contentRegion.show(this.view);
     
      }, 0),


      displayAdminLinks: function() {
        var view = new VideoAdminLinksView();
        this.layout.controlsRegion.show(view);
      },


      /**
       * Teardown.
       */
      onClose: function() {
        
        // This will also happen when the parent layout will
        // be closed.
        this.layout.close();
        this.layout = null;

        // Already done by closing layout but still...
        this.view.close();
        this.view = null;
      }

    });

    return VideoShowController;

});