/**
 * Video list controller.
 *
 * Controls how the video listing is displaying.
 */
define(
  [
    'marionette',
    '../views/video-list-view',
    'services/api'
  ],
  function( Marionette, VideoListView, api ) {

    "use strict";

    var VideoListController = Marionette.Controller.extend({

      /**
       * Returns its view.
       */
      getView: function() {
        if (!this.view) {
          var videos = api.video.list();
          this.view = new VideoListView({collection: videos});
        }

        return this.view;
      },


      /**
       * Teardown.
       */
      onClose: function() {

        // Not really necessary since the region
        // will close it for use...
        this.view.close();
        this.view = null;
      }

    });

    return VideoListController;

});