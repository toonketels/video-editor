/**
 * Main display controller.
 *
 * Initializes displays for main region.
 */
define(
  [
    'marionette',
    'services/state',
    './video-list-controller',
    './video-create-controller',
    './video-show-controller',
    './video-update-controller'
  ],
  function( Marionette, state, VideoListController, VideoCreateController, VideoShowController, VideoUpdateController ) {
    
    "use strict";

    var MainDisplayController = Marionette.Controller.extend({

      /**
       * Initialization.
       */
      initialize: function(options) {
        this.region = options.region;
        this.currentCtrl = null;

        this.initEventListening();

      },


      /**
       * Start listening for events.
       */
      initEventListening: function() {
        this.listenTo(state, 'change:currentAction', this.changeContent, this);
      },



      /**
       * Change the main content.
       */
      changeContent: function(state, value) {
        switch(value) {
          case 'video:list':
            var newCtrl = new VideoListController();
            this.facilitateContentChange(newCtrl);
            break;
          case 'video:create':
            var newCtrl = new VideoCreateController();
            this.facilitateContentChange(newCtrl);
            break;
          case 'video:show':
            var newCtrl = new VideoShowController(state.get('currentVideoDetail'));
            this.facilitateContentChange(newCtrl);
            break;
          case 'video:update':
            var newCtrl = new VideoUpdateController(state.get('currentVideoDetail'));
            this.facilitateContentChange(newCtrl);
            break;
        }
      },


      /**
       * Helper to cleanup existing controller when
       * changing displays.
       */
      facilitateContentChange: function(newController) {
        var previousCtrl;
        if (this.currentCtrl) previousCtrl = this.currentCtrl;

        this.currentCtrl = newController;
        this.region.show(this.currentCtrl.getView());

        // We first allow the region to close the prevousCtrl's view
        // before actually closing the controller itself.
        if (previousCtrl) previousCtrl.close();
      },


      /**
       * Teardown.
       */
      onClose: function() {

        if (this.currentCtrl) currentCtrl.close();
        this.currentCtrl = null;

        // It should already be closed for us, but still...
        this.region.close();
        this.region = null;
      }

    });


    return MainDisplayController;
});