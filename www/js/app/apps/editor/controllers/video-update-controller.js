/**
 * Video update controller.
 *
 * Controls how an individual video is edited.
 */
define(
  [
    'marionette',
    'underscore',
    'services/api',
    'services/state',
    '../views/video-update-view',
    '../views/video-detail-layout',
    '../views/video-admin-links-view',
    '../views/video-editor-controls-view',
    '../views/video-events-view'
  ],
  function( Marionette, _, api, state, VideoUpdateView, VideoDetailLayout, VideoAdminLinksView, VideoEditorControlsView, VideoEventsView ) {

    "use strict";

    var VideoUpdateController = Marionette.Controller.extend({

      /**
       * Initialization.
       */
      initialize: function(options) {
        this.initEventListening();
      },


      /**
       * Start listening for events.
       */
      initEventListening: function() {
        // Whenever there is a different detail selected...
        this.listenTo(state, 'change:currentVideoDetail', this.displayDetail, this);
        this.listenTo(state, 'change:currentVideoDetail', this.displayEditorControls, this);
      },


      /**
       * Returns its view.
       */
      getView: function() {
        if (!this.layout) {
          this.layout = new VideoDetailLayout();

          // Since we cannot show anything in our region unless the region
          // is rendered, we wait for it's dom:refresh event to display
          // the actual content.
          this.listenTo(this.layout, 'dom:refresh', this.displayDetail, this);
          this.listenTo(this.layout, 'dom:refresh', this.displayEditorControls, this);
          this.listenTo(this.layout, 'dom:refresh', this.displayVideoEvents, this);

          // Only once...
          this.listenTo(this.layout, 'dom:refresh', this.displayAdminLinks, this);
        }

        return this.layout;
      },


      /**
       * Actually display the content.
       *
       * For every new detail video, a new view is initialized
       * and displayed.
       *
       * We debounce because in rare occasions, "dom:refresh" and
       * "change:currentvideoDetail" are fired rapidly after each other.
       */
      displayDetail: _.debounce(function() {

        // Initializes new view and removes reference
        // to previous view.
        var video = api.video.show(state.get('currentVideoDetail'));
        this.view = new VideoUpdateView({model: video});

        // Closes the previous view and renderes new one.
        this.layout.contentRegion.show(this.view);
     
      }, 0),


      displayAdminLinks: function() {
        var view = new VideoAdminLinksView();
        this.layout.controlsRegion.show(view);
      },


      displayEditorControls: _.debounce(function() {
        var video = api.video.show(state.get('currentVideoDetail'));
        var view = new VideoEditorControlsView({model: video});

        // Closes the previous view and renders new one.
        this.layout.sideRightRegion.show(view);
      }, 0),


      displayVideoEvents: function() {
        var video = api.video.show(state.get('currentVideoDetail'));
        var videoEvents = video.getEventsCollection();
        var view = new VideoEventsView({collection: videoEvents});

        // We listen for the events being parsed (equivalent to a sync event)
        // @todo: check if this will register this function multiple times...
        this.listenTo(video, 'events:set', this.displayVideoEvents, this);

        // Closes the previous view and renders new one.
        this.layout.sideLeftRegion.show(view);
      },


      /**
       * Teardown.
       */
      onClose: function() {
        
        // This will also happen when the parent layout will
        // be closed.
        this.layout.close();
        this.layout = null;

        // Already done by closing layout but still...
        this.view.close();
        this.view = null;
      }

    });

    return VideoUpdateController;

});