/**
 * Video Detail Layout.
 *
 * Not only specifies a layout but also acts as a controller
 * about what is displayed in it.
 */
define(
  [
    'marionette',
    'tpl!../templates/video-detail-layout.tpl.html'
  ],
  function( Marionette, tpl ) {

    "use strict";

    var VideoDetailLayout = Marionette.Layout.extend({

      template: tpl,

      regions: {
        controlsRegion: "#controls",
        contentRegion: "#content",
        sideLeftRegion: "#side-left",
        sideRightRegion: "#side-right"
      }

    });

    return VideoDetailLayout;
});