/**
 * Video Detail View.
 */
define(
  [
    'marionette',
    'tpl!../templates/video-detail-view.tpl.html',
    'tpl!../templates/loading.tpl.html'
  ],
  function( Marionette, videoDetailTpl, loadingTpl ) {

    "use strict";

    var VideoDetailView = Marionette.ItemView.extend({

      /**
       * On initialization.
       */
      initialize: function(){
        this.initEventListening();
      },


      /**
       * Start listening for events.
       */
      initEventListening: function() {
        // @todo: be more specific in the change events...
        this.listenTo(this.model, 'change', this.render, this);
      },


      /**
       * Descriptions are required, if we have none,
       * we're still fetching the video.
       */
      getTemplate: function() {
        if (this.model.has('description')) {
          return videoDetailTpl;
        } else {
          return loadingTpl;
        }
      },


      /**
       * When our view is actually in the DOM,
       * play the video.
       */
      onDomRefresh: function() {

        // Not for the "loading" view...
        if (this.$('#video').length) {
          this.playVideo();
        }
      },


      /**
       * Plays the video.
       */
      playVideo: function() {

        var video = this.model.displayVideo('#video', '#footnotediv');
        // play the video right away
        video.play();        
      }

    });


    return VideoDetailView;
});