/**
 * Video events view.
 *
 * Display an edit form for each event.
 */
define(
  [
    'marionette',
    './video-events-item-view'
  ],
  function( Marionette, VideoEventsItemView ) {

    "use strict";

    var VideoEventsView = Marionette.CollectionView.extend({

      itemView: VideoEventsItemView,

      tagName: 'ul',

      className: 'unstyled',

      initialize: function(options) {
        this.startEventListening();
      },

      startEventListening: function() {
        this.listenTo(this.collection, 'sort', this.render, this);
      }

    });

    return VideoEventsView;

});