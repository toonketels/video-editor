/**
 * Video editor controls view.
 *
 * Buttons for our editor.
 */
define(
  [
    'marionette',
    'tpl!../templates/video-editor-controls-view.tpl.html',
    'services/router'
  ],
  function( Marionette, tpl, router ) {

    "use strict";

    var VideoEditorControlsView = Marionette.ItemView.extend({

      template: tpl,

      initialize: function() {
        this.keepReferenceVideo();
        this.keepReferenceCollection();
      },


      /**
       * Keep a reference to the popcorn video object.
       */
      keepReferenceVideo: function() {
        this.video = this.model.getCurrentVideo();

        this.listenTo(this.model, 'video:instantiated', function(video) {
          this.video = video;
        });
      },

      
      /**
       * Keep a reference to the video events collection.
       */
      keepReferenceCollection: function() {
        this.collection = this.model.getEventsCollection();

        this.listenTo(this.model, 'events:set', function(){
          this.collection = this.model.getEventsCollection();
        });
      },


      events: {
        'click .add-annotation': 'addEvent',
        'click .save': 'saveVideo',
        'click .view-changes': 'viewChanges'
      },


      /**
       * Add a new event to the video.
       */
      addEvent: function(){
        var start = this.video ? Math.round(this.video.currentTime()) : 0;
        this.collection.create({start: start, end: start + 3});
      },


      saveVideo: function() {
        this.model.save();
      },

      onClose: function() {
        this.model = null;
        this.collection = null;
      },

      viewChanges: function() {
        router.navigate(this.model.url(), {trigger: true});
      }

    });

    return VideoEditorControlsView;

});