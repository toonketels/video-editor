/**
 * Video create view.
 *
 * Display a form to add a new video.
 */
define(
  [
    'marionette',
    'tpl!../templates/video-create-view.tpl.html',
    'services/api',
    'services/router'
  ],
  function( Marionette, tpl, api, router ){

    "use strict";

    var VideoCreateView = Marionette.ItemView.extend({

      className: 'span12',

      template: tpl,

      events: {
        'click .submit': 'createVideo'
      },

      /**
       * Initialization.
       */
      initialize: function() {
        this.model = api.video.getNewEmptyVideo();

        this.initEventListening();
      },


      /**
       * Listen for specific events.
       */
      initEventListening: function() {
        this.listenTo(this.model, 'change:_id', this.goToDetail, this);
        this.listenTo(this.model, 'invalid', this.displayValidationErrors, this);
      },


      /**
       * Grabs the values from the form and
       * tries to save the video.
       */
      createVideo: function(ev) {
        ev.preventDefault();

        var title = this.$('input#title').val();
        var url = this.$('input#url').val();
        var description =  this.$('textarea#description').val();
        var provider =  this.$('input:radio[name=videoProvider]:checked').val();

        this.model.set({title: title, url: url, type: provider, description: description});

        // Save will call validate automatically. We watch
        // for validation error events.
        api.video.create(this.model);
      },


      /**
       * If we get an '_id' back, we know our model is
       * successfully processed by backend and we go to
       * the detail.
       */
      goToDetail: function() {
        router.navigate(this.model.url(), {trigger: true});
      },


      /**
       * Display the validation errors.
       */
      displayValidationErrors: function(model, value) {
        this.$('.validation ul').html('<li>'+value+'</li>');
      }

    });

    return VideoCreateView;

});