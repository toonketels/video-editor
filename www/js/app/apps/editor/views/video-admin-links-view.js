/**
 * Video Admin Links View.
 *
 * Displays view/edit/delete links.
 */
define(
  [
    'marionette',
    'backbone',
    'tpl!../templates/video-admin-links-view.tpl.html',
    'services/state',
    'services/router',
    'services/api',
    'bootstrap'
  ],
  function( Marionette, Backbone, tpl, state, router, api ) {

    "use strict";

    var VideoAdminLinksView = Marionette.ItemView.extend({

      template: tpl,

      onRender: function() {

        switch(state.get('currentAction')) {
          case 'video:update':
            this.$('button').removeClass('active');
            this.$('.edit').addClass('active');
            break;
          case 'video:show':
            this.$('button').removeClass('active');
            this.$('.view').addClass('active');
            break;
        }
      },

      events: {
        'click .view': 'view',
        'click .edit:not(.active)': 'edit',
        'click .delete': 'askToDestroy',
        'click .confirm-delete': 'destroy'
      },

      view: function() {
        router.navigate('video/'+state.get('currentVideoDetail'), {trigger: true});
      },

      edit: function() {
        router.navigate('video/'+state.get('currentVideoDetail')+'/edit', {trigger: true});

      },

      askToDestroy: function() {
        $('#myModal').modal('show');
      },

      destroy: function() {

        $('#myModal').modal('hide');
        var video  = api.video.show(state.get('currentVideoDetail'));
        api.video.destroy(video);

        // We should check if successfull.
        router.navigate('video', {trigger: true});
      }

    });

    return VideoAdminLinksView;
});