/**
 * Displays a single event in a form to be edited.
 */
define(
  [
    'marionette',
    'tpl!../templates/video-events-item-view.tpl.html'
  ],
  function( Marionette, tpl ) {

    "use strict";

    var VideoEventsItemView = Marionette.ItemView.extend({

      template: tpl,

      tagName: 'li',

      className: 'video-events-item',

      events: {
        'click .save': 'updateEvent',
        'click .del': 'deleteEvent'
      },

      updateEvent: function(ev) {
        var start = this.$('.val-start').val();
        var end = this.$('.val-end').val();
        var text = this.$('.val-text').val();

        // @todo: check validation errors
        this.model.set({start: start, end: end, text: text});

      },

      deleteEvent: function(ev) {
        console.log(this.model);
        this.model.collection.remove(this.model);
      },

      serializeData: function() {
        return {
          text: this.model.get('text'),
          start: this.model.get('start'),
          end: this.model.get('end')
        }
      }

    });


    return VideoEventsItemView;

});