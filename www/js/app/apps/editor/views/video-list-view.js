/**
 * Video list view.
 *
 * Displays all videos.
 */
define(
  [
    'marionette',
    './video-list-item-view'
  ],
  function( Marionette, VideoListItemView ) {

    "use strict";

    var VideoListView = Marionette.CollectionView.extend({

      itemView: VideoListItemView,

      tagName: 'ul',

      className: 'span12 unstyled video-list-view'

    });

    return VideoListView;

});
