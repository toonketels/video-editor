/**
 * Video list item view.
 *
 * Displays a single video in the list.
 */
define(
  [
    'marionette',
    'tpl!../templates/video-list-item-view.tpl.html',
    'services/router'
  ],
  function( Marionette, tpl, router ) {

    "use strict";

    var VideoListView = Marionette.ItemView.extend({

      template: tpl,

      tagName: 'li',

      events: {
        'click': 'goToDetail'
      },

      goToDetail: function() {
        router.navigate(this.model.url(), {trigger: true});
      }

    });

    return VideoListView;

});
