/**
 * Editor App.
 *
 * Allows for browsing, creating and editing videos.
 *
 * Controls the main region.
 */
define(
  [
    'marionette',
    './controllers/main-display-controller'
  ],
  function( Marionette, MainDisplayController ) {

    "use strict";

    var EditorApp = Marionette.Controller.extend({

      /**
       * App initialization.
       */
      initialize: function(options) {
        this.region = options.region

        this.ctrl = {};
        this.ctrl.mainDisplay = this.initMainDisplay(this.region);
      },


      /**
       * Initializes main region controller which is responsible
       * for what is displayed in the main region.
       */
      initMainDisplay: function(region) {
        return new MainDisplayController({region: region});
      },


      /**
       * Teardown.
       */
      onClose: function() {

        this.ctrl.mainDisplay.close();
        this.ctrl = null;

        this.region.close();
        this.region = null;
      }

    });
    
    return EditorApp;
});