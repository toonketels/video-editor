/**
 * Video Collection.
 */
define(
  [
    'backbone',
    '../models/video-model'
  ],
  function( Backbone, VideoModel ) {

    "use strict";

    var VideoCollection = Backbone.Collection.extend({

      url: "video",

      model: VideoModel

    });

    return VideoCollection;
});