/**
 * Video events collection.
 */
define(
  [
    'backbone',
    '../models/video-event-model'
  ],
  function( Backbone, VideoEventModel ) {

    "use strict";

    var VideoEventsCollection = Backbone.Collection.extend({

      model: VideoEventModel,

      initialize: function() {
        // Ensure the collection is resorted whenever the start values change
        this.on('change:start', this.sort, this);
      },

      /**
       * Keep sorted on start time.
       */
      comparator: 'start',
      
      /**
       * Override create to not save it to the server
       * since we only want it to be saved as part of a
       * deliberate save on the video itself.
       */
      create: function(model, options) {
        options = options ? _.clone(options) : {};
        if (!(model = this._prepareModel(model, options))) return false;
        if (!options.wait) this.add(model, options);
        var collection = this;
        var success = options.success;
        options.success = function(resp) {
          if (options.wait) collection.add(model, options);
          if (success) success(model, resp, options);
        };
      }

    });

    return VideoEventsCollection;

});