/**
 * Video API.
 *
 * Provides a public API to use by other modules.
 *
 * This is exposed as part of the api service in `servides/api`,
 * so dont require this app directly.
 *
 * @todo: Allow options objects to be passed to api with error/success
 *        callbacks to be called after each request.
 *        Add default success/error handler by showing message to user
 *        (via new message app).
 */
define(
  [
    './collections/video-collection',
    './models/video-model',
    'services/error-logger'
  ],
  function( VideoCollection, VideoModel, errorLogger ) {

    "use strict";


    /**
     * We have a statefull videoCollection api.
     * There should be only one collection.
     */
    var videoCollection;


    /**
     * Private video methods.
     */
    var _video = {
      getVideoCollection: function() {
        if (!videoCollection) {
          videoCollection = new VideoCollection();
          videoCollection.fetch({error: function(collection, response, options) {
            errorLogger.log('Fetch for video collection was not successful. Server send response: ' + JSON.stringify(response));
          }});
        }
        return videoCollection;
      }
    };



    /**
     * Public api.
     */
    var videoApi = {

      /**
       * List all videos.
       */
      list: function() {
        return _video.getVideoCollection();
      },


      /**
       * Show a single model.
       */
      show: function(id) {
        var videoCollection = _video.getVideoCollection(),
            video = videoCollection.get(id);
        if (!video) {
          video = new videoCollection.model();
          video.id = id;
          video.fetch({
            success: function(model, response, options) {
              videoCollection.add(video);
            },
            error: function(model, response, options) {
              errorLogger.log('Error in fetching model with id ' + id);
              // @todo handle situation
            }
          });
        }
        return video;
      },


      /**
       * Create a single model.
       */
      create: function(attributes) {
        var videoCollection = _video.getVideoCollection(),
            video = videoCollection.create(attributes, {
              wait: true,
              error: function(model, xhr, options) {
                errorLogger.log('Error creating new model with attributes ' +  JSON.stringify(attributes));
                // @todo: handle situation
              }
            });
        return video;
      },


      /**
       * Update an existing video.
       */
      update: function(video) {
        video.save({
          error: function(model, xhr, options) {
            errorLogger.log('Unable to video to server for model with attributes' +  JSON.stringify(attributes) + ' and server response ' + JSON.stringify(xhr));
            // @todo: handle situation
          }
        });
      },


      /**
       * Destroy a video.
       */
      destroy: function(model) {
        model.destroy({
          wait: true,
          error: function(model, xhr, options) {
            errorLogger.log('Unable to destroy on server the model with id ' +  model.id + ' and server response ' + JSON.stringify(xhr));
            // @todo: handle situation
          }
        });
      },


      /**
       * Return a new empty video.
       */
      getNewEmptyVideo: function() {
        return new VideoModel();
      }

    };

    return videoApi;

});