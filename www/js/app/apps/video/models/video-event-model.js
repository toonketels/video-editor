/**
 * Video event model.
 *
 * Represents a single popcorn event.
 */
define(
  [
    'backbone'
  ],
  function( Backbone ) {

    "use strict";

    var VideoEventModel = Backbone.Model.extend({

      defaults: {
        start: 0,
        end: 0,
        text: "Pop this!",
        target: "footnotediv"
      },

      idAttribute: '_id',


      /**
       * Override to json to represent it in a format
       * popcorn is used to.
       */
      toJSON: function(options) {

        // Get the attributes;
        var copy =  _.clone(this.attributes),
            newObj = {};

        // Set the _id if we have one
        if (copy._id) {
            newObj._id = copy._id;
            delete copy._id;
        }
        // Set footnote
        newObj.footnote = copy;

        return newObj;
      }

    });

    return VideoEventModel;

});