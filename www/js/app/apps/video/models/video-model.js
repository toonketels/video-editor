/**
 * Video Model.
 */
define(
  [
    'backbone',
    'underscore',
    '../collections/video-events-collection',
    './video-event-model',
    'popcorn'
  ],
  function( Backbone, _, VideoEventsCollection, VideoEventModel, Popcorn ) {

    "use strict";

    var VideoModel = Backbone.Model.extend({

      urlRoot: 'video',

      idAttribute: '_id',

      defaults: {
        title: "New video"
      },


      /**
       * Validation function, called before save.
       */
      validate: function(attrs, options) {

        if (!attrs.title) {
          return 'Please provide a title';
        }

        if (!attrs.url) {
          return 'Please a url to a youtube or video video';
        }

        if (!/^http:\/\/.*\.[a-z]{2,3}\/.*$/i.test(attrs.url)) {
          return 'Please return a correct url';
        }

        if (!/^(youtube||vimeo)$/.test(attrs.type)) {
          return 'Please state your provider';
        }
      },

      /**
       * Returns a new video event to be edited.
       */
      getNewEmptyVideoEvent: function(options) {
        return new VideoEventModel(options);
      },

      /**
       * Add an event to this video.
       *
       * We can pass an attributes hash or video event model.
       */
      addEvent: function(options) {

        if (!(options instanceof VideoEventModel)) {
          options = new VideoEventModel(options);
        }

        var eventsCol = this.getEventsCollection();
        eventsCol.add(options);

        return options;
      },


      /**
       * Returns the eventsCollection for this video.
       *
       * Lazely instantiate the collection.
       */
      getEventsCollection: function() {
        if (!this._eventsCollection) {
          this._eventsCollection = new VideoEventsCollection();
        }

        return this._eventsCollection;
      },

      /**
       * Set the events collection.
       */
      setEventsCollection: function(data) {
        this._eventsCollection = new VideoEventsCollection(data);
        // Trigger custom event on model to state the collection is set.
        this.trigger('events:set');

        return this._eventsCollection;
      },

     
      /**
       * Override the default toJSON implementation by
       * stringifying the video events collection as the data
       * attribute.
       */
      toJSON: function(options) {
        var copy =  _.clone(this.attributes);
        var eventsCollection =  this.getEventsCollection();
        copy.data =  eventsCollection.toJSON();
        return copy;
      },


      /**
       * Override parse so it instantiates our video model
       * and create a real collection for the video events.
       */
      parse: function(response, options) {

        // data represents our video events array      
        var eventsCollection = response.data;
        delete response.data;

        // Create propper model attribute hashes...
        var ev = _.map(eventsCollection, function(item) {
          var newObj = item.footnote;
          newObj._id = item._id;

          return newObj;
        });

        // Instantiate events collection;
        this.setEventsCollection(ev);

        // Return hash with video model attributes;
        return response;
      },

      
      // @todo: trigger events on model when events change...


      /**
       * Attaches a video the the dom.
       *
       * For the moment, only one video instance is allowed,
       * not happy with the way the instance is returned.
       */
      displayVideo: function(videoSelector, annotateSelector) {

        this.video = Popcorn[this.get('type')](videoSelector, this.get('url'));

        var col = this.getEventsCollection();

        col.each(function(videoEvent) {
          this.video.footnote({
            start: videoEvent.get('start'),
            end: videoEvent.get('end'),
            target: annotateSelector,
            text: videoEvent.get('text')
          });
        }, this);

        this.trigger('video:instantiated', this.video);
        this.trigger('all', 'video:instantiated', this.video);

        return this.video;

      },


      /**
       * Returns the current video instance.
       */
      getCurrentVideo: function() {
        return (this.video) ? this.video : false;
      }


    });

    return VideoModel;
});